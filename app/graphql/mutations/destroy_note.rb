# frozen_string_literal: true

module Mutations
  class DestroyNote < Mutations::BaseMutation
    field :id, ID, null: true

    argument :id, ID, required: true

    def resolve(id:)
      note = Note.find(id)
      note.destroy
      { id: id }
    rescue ActiveRecord::RecordNotFound => _e
      GraphQL::ExecutionError.new('Note does not exist.')
    end
  end
end
